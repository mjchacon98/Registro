/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estudiante;

/**
 *
 * @author Melvin
 */
public class NuevoEstudiante {
    public String Nombre ;
    public int Edad;
    public String Grado;
    
    public NuevoEstudiante(String Nombre,int Edad,String Grado){
        this.Nombre = Nombre;
        this.Edad= Edad;
        this.Grado= Grado;
    }
    
    @Override
    public String toString()
    {
        String s = "**Estudiante**\n";
        s += "Nombre: "+this.Nombre+ "\n";
        s+= "Edad: "+ this.Edad+ "\n";
        s+= "Grado: "+ this.Grado+"\n";
        return s;
    }
}
